package com.bankele.examen.domain;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CreditoTest {

    private static final Logger LOG = LoggerFactory.getLogger(CreditoTest.class);
    
    @Test
    public void amortizacionTest() {
        
        Credito credito =  new Credito(650000, 1, 15);

        assertEquals(13, credito.generarTablaAmortizacion().size());
    }
    
}
