package com.bankele.examen.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.bankele.examen.domain.Credito;
import com.bankele.examen.domain.Periodo;

@Service
public class CreditoServices {

    public List<Periodo> calcularAmortizacion(double monto, int plazo, double tasa) {
        
        if(monto <= 0 || tasa <= 0) {
            throw new IllegalArgumentException("El monto y la tasa deben ser mayores a 0");
        }
        
        if(plazo < 1) {
            throw new IllegalArgumentException("El plazo debe ser al menos de 1 año");
        }
        
        Credito credito =  new Credito(monto, plazo, tasa);
        
        return credito.generarTablaAmortizacion();
    }
    
}
