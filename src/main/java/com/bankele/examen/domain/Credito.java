package com.bankele.examen.domain;

import java.util.ArrayList;
import java.util.List;

public class Credito {

    private double monto;
    private int plazoAnios;
    private double tasa;
    private double periodos;
    
    public Credito(double monto, int plazoAnios, double tasa) {
        this.monto = monto;
        this.plazoAnios = plazoAnios;
        this.tasa = tasa / 100;
        this.periodos = plazoAnios * 12;
    }
    
    public List<Periodo> generarTablaAmortizacion() {
        
        List<Periodo> tablaAmortizacion = new ArrayList<>();
        
        Periodo periodoCero =  new Periodo(0, 0, 0, 0, monto);
        tablaAmortizacion.add(periodoCero);
        
        double saldoInicial = monto;
        double saldoRestante = saldoInicial;
        
        double pago = this.calcularCuota();
        
        for(int i = 1; i <= periodos; i++) {
            
            double interes = saldoRestante * (tasa / 12);
            
            if(saldoRestante < pago) {
                pago = saldoRestante;
                saldoRestante = 0;
            } else {
                saldoRestante = saldoInicial + interes - pago;
            }
            
            double amortizacion = pago - interes;
            
            saldoInicial = saldoRestante;
            Periodo actual = new Periodo(i, pago, interes, amortizacion, saldoRestante);
            tablaAmortizacion.add(actual);
        }
        
        return tablaAmortizacion;
    }
    
    public double calcularCuota() {
        double tasaMensual = tasa / 12;
        double factor = Math.pow(1 + tasaMensual, periodos);
        
        return monto * ((factor * tasaMensual) / (factor - 1));
    }
    
    public double getMonto() {
        return monto;
    }
    
    public double getPlazoAnios() {
        return plazoAnios;
    }
    
    public double getTasa() {
        return tasa;
    }
    
}
