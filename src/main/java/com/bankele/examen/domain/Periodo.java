package com.bankele.examen.domain;

public class Periodo {

    private int numero;
    private double cuotaAPagar;
    private double interes;
    private double amortizacion;
    private double saldoRestante;
    
    public Periodo(int numero, double cuotaAPagar, double interes
            , double amortizacion, double saldoRestante) {
        this.numero = numero;
        this.cuotaAPagar = cuotaAPagar;
        this.interes = interes;
        this.amortizacion = amortizacion;
        this.saldoRestante = saldoRestante;
    }
    
    public int getNumero() {
        return numero;
    }

    public double getCuotaAPagar() {
        return cuotaAPagar;
    }

    public double getInteres() {
        return interes;
    }

    public double getAmortizacion() {
        return amortizacion;
    }

    public double getSaldoRestante() {
        return saldoRestante;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("Periodo [numero=");
        builder.append(numero);
        builder.append(", cuotaAPagar=");
        builder.append(cuotaAPagar);
        builder.append(", interes=");
        builder.append(interes);
        builder.append(", amortizacion=");
        builder.append(amortizacion);
        builder.append(", saldoRestante=");
        builder.append(saldoRestante);
        builder.append("]");
        return builder.toString();
    }
}
