package com.bankele.examen.resources;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.bankele.examen.domain.Periodo;
import com.bankele.examen.services.CreditoServices;

@RestController
public class AmortizacionResource {

    private static final Logger LOG = LoggerFactory.getLogger(AmortizacionResource.class);
    
    @Autowired
    private CreditoServices creditoServices;
    
    @GetMapping("/amortizacion")
    public List<Periodo> obtenerTablaAmortizacion(@RequestParam double monto
            , @RequestParam int plazoAnios, @RequestParam double tasa) {
        try {
            LOG.debug("Parametros de crédito: {}, {}, {}", monto, plazoAnios, tasa);
            return creditoServices.calcularAmortizacion(monto, plazoAnios, tasa);
        } catch (IllegalArgumentException ex) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, ex.toString(), ex);
        }
    }
    
}
